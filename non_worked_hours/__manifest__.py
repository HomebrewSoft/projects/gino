{
    "name": "Non worked hours",
    "version": "14.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "hr_attendance",
        "resource",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/hr_attendance.xml",
        "views/resource_calendar_attendance.xml",
    ],
}
