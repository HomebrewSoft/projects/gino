from datetime import datetime
from re import A

import pytz

from odoo import _, api, fields, models
from odoo.tools.misc import attrgetter


class HRAttendance(models.Model):
    _inherit = "hr.attendance"

    shift_id = fields.Many2one(
        comodel_name="resource.calendar.attendance",
        compute="_compute_shift_id",
    )
    non_worked_hours = fields.Float(
        compute="_compute_non_worked_hours",
        string="Non-worked hours",
        store=True,
    )

    @api.depends("check_in")
    def _compute_shift_id(self):
        for attendance in self:
            employee = attendance.employee_id
            date = fields.Datetime.context_timestamp(self, attendance.check_in)
            attendance.shift_id = employee.resource_calendar_id.get_shift(date)

    @api.depends("worked_hours", "shift_id", "employee_id.tz")
    def _compute_non_worked_hours(self):
        for attendance in self:
            if not attendance.shift_id:
                attendance.non_worked_hours = 0
            else:
                hour_from = int(attendance.shift_id.hour_from)
                minute_from = int((attendance.shift_id.hour_from - hour_from) * 60)
                tz = pytz.timezone(attendance.employee_id.tz)
                entrance = attendance.check_in.replace(hour=hour_from, minute=minute_from, tzinfo=tz)
                check_in = attendance.check_in.replace(tzinfo=pytz.UTC)
                check_in = check_in.astimezone(tz)
                check_in = check_in.replace(tzinfo=None)
                entrance = entrance.replace(tzinfo=None)
                attendance.non_worked_hours = (check_in - entrance).seconds / 60 / 60
