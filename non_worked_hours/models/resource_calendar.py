from datetime import datetime
from odoo import _, api, fields, models


class ResourceCalendar(models.Model):
    _inherit = "resource.calendar"

    def get_shift(self, date: datetime):
        """Get shift for the given hour

        Args:
            date (datetime): Date to get shift for

        Returns:
            resource.calendar.attendance: Shift for the given date
        """
        hour = date.hour
        day_of_week = str(date.weekday())
        shifts = self.attendance_ids.filtered(
            lambda shift: shift.hour_from <= hour and shift.dayofweek == day_of_week
        )
        return shifts[-1] if shifts else None
