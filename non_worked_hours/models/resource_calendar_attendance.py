from odoo import _, api, fields, models


class ResourceCalendarAttendance(models.Model):
    _inherit = "resource.calendar.attendance"

    total_hours = fields.Float(
        compute="_compute_total_hours",
    )
    real_hour_to = fields.Float(
        compute="_compute_real_hour_to",
    )

    @api.depends("hour_from", "hour_to")
    def _compute_total_hours(self):
        for attendance in self:
            attendance.total_hours = attendance.real_hour_to - attendance.hour_from

    def _compute_real_hour_to(self):
        for attendance in self:
            if not attendance.hour_to:
                attendance.real_hour_to = attendance.hour_to
                continue
            attendance.real_hour_to = attendance.hour_to
            if attendance.hour_to < attendance.hour_from:
                attendance.real_hour_to += 24
